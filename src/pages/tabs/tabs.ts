import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { ChatPage } from '../chat/chat';
import { ConfigPage } from '../config/config';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  home = HomePage;
  chat = ChatPage;
  config = ConfigPage;

  constructor() {

  }
}
