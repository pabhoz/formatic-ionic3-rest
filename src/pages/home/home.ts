import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ItemsProvider } from '../../providers/provider';
import { DetallePage } from '../detalle/detalle';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  items: any = [];
  detalle: any = DetallePage;
  init = 0;
  limit = 10;

  constructor(public navCtrl: NavController,private itemsService: ItemsProvider,public toastCtrl: ToastController) {
    /*for (let i = 0; i < 30; i++) {
      this.items.push( "Item "+(this.items.length+1) );
    }*/
    this.itemsService.getAll(this.init,this.limit).subscribe(()=>{
      this.items = this.itemsService.items;
    });
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
    this.init+= 10;
    this.itemsService.getAll(this.init,this.limit).subscribe(()=>{
      if(this.itemsService.items.length > 0){
        this.items = this.items.concat(this.itemsService.items);
      }else{
        const toast = this.toastCtrl.create({
          message: 'No existen más items en la tienda',
          duration: 3000
        });
        toast.present();
      }
      infiniteScroll.complete();
    });
    }, 700);

  }

}
