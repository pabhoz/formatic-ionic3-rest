import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { APIREST } from '../provider';

/*
  Generated class for the ItemsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ItemsProvider {

  endpoint: string = `${APIREST}Items/`;
  items: any = [];

  constructor(public http: HttpClient) {
    
  }

  getAll(init,limit){
    return this.http.get(`${this.endpoint}?init=${init}&limit=${limit}`,{}).pipe(map((items) => {
      this.items = items;
    }));
  }

  getItem(id){

  }

}
